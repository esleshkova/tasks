function letterAlphabet(string) {
    let array=[], arr=[];
    for (let i=0; i<string.length; i++){
        array.push(string.charCodeAt(i));
    }
    function compareNumbers(a, b){
        return a-b;
    }
    array.sort(compareNumbers);
    for (let j=0; j<array.length-1; j++){
        if (array[j+1]-array[j]>1){
            for (let k=1; k<array[j+1]-array[j]; ++k){
                arr.push(String.fromCharCode(array[j]+k));
            }
        }
    }
    return arr.join("");
}
console.log(letterAlphabet("phone"));