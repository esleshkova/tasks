function findEl(arr, func) {
    return arr.find(func);
}
findEl([1, 5, 10, 19, 23, 30], function(num) { return num % 2 === 0; }) ;