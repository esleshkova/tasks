function findLargest(...arr){
    let array=[];
    for(let i=0; i<arr.length; ++i){
        array.push(Math.max.apply(null, arr[i]));
    }
    return array;
}
console.log(findLargest([3,67, 3], [6, 7, 98], [4, 5, 8], [5, 45, 5]));